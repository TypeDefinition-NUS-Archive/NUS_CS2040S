// Lim Ngian Xin Terry
// A0218430N

import java.util.*;

public class Runner {
    public final String name;
    public final double leg1Time;
    public final double legNTime;

    public Runner(String name, double leg1Time, double legNTime) {
        this.name = name;
        this.leg1Time = leg1Time;
        this.legNTime = legNTime;
    }

    public String toString() {
        return name + Double.toString(leg1Time) + " " + Double.toString(legNTime);
    }
}