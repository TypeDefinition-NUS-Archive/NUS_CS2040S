// Lim Ngian Xin Terry
// A0218430N

import java.util.*;
import java.io.*;

class GridNode implements Comparable<GridNode> {
    private static int currentID = 0;

    public final int id;
    public final int height; // Height of the pile of money.
    public final int row, col;
    public final int distanceToDest; // How far this node is to the destination node.
    public int ladder; // The ladder length required to reach this node.
    public boolean visited;
    public boolean enqueued;

    public GridNode(int height, int row, int col, int distanceToDest) {
        this.id = currentID++;
        this.row = row;
        this.col = col;
        this.ladder = Integer.MAX_VALUE;
        this.height = height;
        this.distanceToDest = distanceToDest;
        this.visited = this.enqueued = false;
    }

    public int compareTo(GridNode other) {
        int result = ladder - other.ladder;
        if (result == 0) {
            result = distanceToDest - other.distanceToDest;
        }
        if (result == 0) {
            result = id - other.id;
        }
        return result;
    }
}

public class Asn4A {
    // Modified A* Search
    private static int getShortestLadder(GridNode grid[][]) {
        TreeSet<GridNode> list = new TreeSet<>();
        list.add(grid[0][0]);
        grid[0][0].ladder = 0;
        grid[0][0].enqueued = true;

        while (!list.isEmpty()) {
            GridNode currentNode = list.pollFirst();
            currentNode.visited = true;

            // We have reached the destination.
            if (currentNode.distanceToDest == 0) {
                return currentNode.ladder;
            }

            // Add Neighbours to the list.
            up: {
                int neighbourRow = currentNode.row - 1;
                int neighbourCol = currentNode.col;
                if (neighbourRow < 0) {
                    break up; // Can't break up if you've never had a relationship... HAHAHAHAhahahahaha...
                              // *Sniffs*... *Cries*...
                }

                // Check if the neighbour has already been visited. If it has already been
                // visited, it means that there was already a shorter path to it before this.
                GridNode neighbourNode = grid[neighbourRow][neighbourCol];
                if (neighbourNode.visited) {
                    break up;
                }

                // Check if there is a cheaper path to reach the neighbour.
                int ladder = Math.max(neighbourNode.height - currentNode.height, currentNode.ladder);
                if (ladder < neighbourNode.ladder) {
                    if (neighbourNode.enqueued) {
                        list.remove(neighbourNode);
                        neighbourNode.enqueued = false;
                    }
                    neighbourNode.ladder = ladder;
                }
                if (!neighbourNode.enqueued) {
                    list.add(neighbourNode);
                    neighbourNode.enqueued = true;
                }
            }

            down: {
                int neighbourRow = currentNode.row + 1;
                int neighbourCol = currentNode.col;
                if (neighbourRow >= grid.length) {
                    break down; // I do this everyday.
                }

                GridNode neighbourNode = grid[neighbourRow][neighbourCol];
                if (neighbourNode.visited) {
                    break down;
                }

                int ladder = Math.max(neighbourNode.height - currentNode.height, currentNode.ladder);
                if (ladder < neighbourNode.ladder) {
                    if (neighbourNode.enqueued) {
                        list.remove(neighbourNode);
                        neighbourNode.enqueued = false;
                    }
                    neighbourNode.ladder = ladder;
                }
                if (!neighbourNode.enqueued) {
                    list.add(neighbourNode);
                    neighbourNode.enqueued = true;
                }
            }

            left: {
                int neighbourRow = currentNode.row;
                int neighbourCol = currentNode.col - 1;
                if (neighbourCol < 0) {
                    break left;
                }

                GridNode neighbourNode = grid[neighbourRow][neighbourCol];
                if (neighbourNode.visited) {
                    break left;
                }

                int ladder = Math.max(neighbourNode.height - currentNode.height, currentNode.ladder);
                if (ladder < neighbourNode.ladder) {
                    if (neighbourNode.enqueued) {
                        list.remove(neighbourNode);
                        neighbourNode.enqueued = false;
                    }
                    neighbourNode.ladder = ladder;
                }
                if (!neighbourNode.enqueued) {
                    list.add(neighbourNode);
                    neighbourNode.enqueued = true;
                }
            }

            right: {
                int neighbourRow = currentNode.row;
                int neighbourCol = currentNode.col + 1;
                if (neighbourCol >= grid[0].length) {
                    break right;
                }

                GridNode neighbourNode = grid[neighbourRow][neighbourCol];
                if (neighbourNode.visited) {
                    break right;
                }

                int ladder = Math.max(neighbourNode.height - currentNode.height, currentNode.ladder);
                if (ladder < neighbourNode.ladder) {
                    if (neighbourNode.enqueued) {
                        list.remove(neighbourNode);
                        neighbourNode.enqueued = false;
                    }
                    neighbourNode.ladder = ladder;
                }
                if (!neighbourNode.enqueued) {
                    list.add(neighbourNode);
                    neighbourNode.enqueued = true;
                }
            }
        }

        return -1;
    }

    public static void main(String _args[]) throws Exception {
        // Initialise IO.
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        PrintWriter printWriter = new PrintWriter(System.out);

        // Read M & N.
        String firstLine[] = bufferedReader.readLine().split(" ");
        int m = Integer.parseInt(firstLine[0]);
        int n = Integer.parseInt(firstLine[1]);

        // Populate grid.
        GridNode grid[][] = new GridNode[m][n];
        for (int row = 0; row < m; ++row) {
            String line[] = bufferedReader.readLine().split(" ");
            for (int col = 0; col < n; ++col) {
                grid[row][col] = new GridNode(Integer.parseInt(line[col]), row, col, (m - row - 1) + (n - col - 1));
            }
        }

        printWriter.println(getShortestLadder(grid));

        // Close IO.
        bufferedReader.close();
        printWriter.close();
    }
}