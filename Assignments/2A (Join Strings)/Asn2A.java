// Lim Ngian Xin Terry
// A0218430N

import java.util.*;
import java.io.*;

public class Asn2A {
    public static void main(String[] _args) throws IOException {
        // Create BufferedReader
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));

        // Read Strings
        int numStrings = Integer.parseInt(bufferedReader.readLine());
        String strings[] = new String[numStrings];
        for (int i = 0; i < numStrings; i++) {
            strings[i] = bufferedReader.readLine();
        }

        // Read Combinations
        int strIndex = 0; // Index of the current string to print.
        int printOrder[] = new int[numStrings];
        int tailIndices[] = new int[numStrings];
        Arrays.fill(printOrder, -1);
        Arrays.fill(tailIndices, -1);
        for (int i = 0; i < numStrings - 1; i++) {
            StringTokenizer tokenizer = new StringTokenizer(bufferedReader.readLine(), " ");
            int a = Integer.parseInt(tokenizer.nextToken()) - 1;
            int b = Integer.parseInt(tokenizer.nextToken()) - 1;

            strIndex = a;

            if (tailIndices[a] == -1) {
                printOrder[a] = b;
            } else {
                printOrder[tailIndices[a]] = b;
            }

            if (tailIndices[b] == -1) {
                tailIndices[a] = b;
            } else {
                tailIndices[a] = tailIndices[b];
            }
        }

        // Close BufferedReader
        bufferedReader.close();

        // Create PrintWriter
        PrintWriter printWriter = new PrintWriter(System.out);

        // Print Answer
        while (strIndex != -1) {
            printWriter.print(strings[strIndex]);
            strIndex = printOrder[strIndex];
        }
        printWriter.println();

        // Close PrintWriter
        printWriter.close();
    }
}