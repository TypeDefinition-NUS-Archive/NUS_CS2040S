// Lim Ngian Xin Terry
// A0218430N

import java.util.*;

class Card {
    public long buyLoss;
    public long sellGain;
    public long quantity;

    public Card() {
        this.buyLoss = 0;
        this.sellGain = 0;
        this.quantity = 0;
    }
}

public class Asn1B {
    public static void main(String[] _args) {
        Scanner scanner = new Scanner(System.in);

        // Get N, T & K.
        int n = scanner.nextInt();
        int t = scanner.nextInt();
        int k = scanner.nextInt();
        scanner.nextLine();

        // Get the quantity of each card.
        Card[] deck = new Card[t];
        for (int i = 0; i < n; ++i) {
            int number = scanner.nextInt();
            Card card = deck[number - 1];
            if (card == null) {
                card = new Card();
                deck[number - 1] = card;
            }
            ++card.quantity;
        }
        scanner.nextLine();

        // Get the Buy Price & Sell Price of each card.
        for (int i = 0; i < t; ++i) {
            long buyPrice = scanner.nextLong();
            long sellPrice = scanner.nextLong();

            if (deck[i] == null) { deck[i] = new Card(); }
            Card card = deck[i];

            card.buyLoss = buyPrice * (2 - card.quantity);
            card.sellGain = sellPrice * card.quantity;
            if (!scanner.hasNextLine()) {
                break;
            }
            scanner.nextLine();
        }
        scanner.close();

        // Sort the deck.
        Arrays.sort(deck, (Card a, Card b) -> {
            if ((a.buyLoss + a.sellGain) < (b.buyLoss + b.sellGain)) {
                return -1;
            }
            if ((a.buyLoss + a.sellGain) > (b.buyLoss + b.sellGain)) {
                return 1;
            }
            return 0;
        });

        // Calculate profit.
        long profit = 0;
        for (int i = 0; i < k; ++i) {
            profit -= deck[i].buyLoss;
        }
        for (int i = k; i < deck.length; ++i) {
            profit += deck[i].sellGain;
        }

        // Print result.
        System.out.println(Long.toString(profit));
    }
}