// Lim Ngian Xin Terry
// A0218430N

import java.util.*;
import java.io.*;

class Tile {
    public final ArrayList<Tile> outEdges;
    public final ArrayList<Tile> inEdges;
    public final int index;
    public int sccIndex;

    public Tile(int index) {
        this.outEdges = new ArrayList<>();
        this.inEdges = new ArrayList<>();
        this.index = index;
        this.sccIndex = -1;
    }

    public boolean isIsolated() {
        for (Tile next : inEdges) {
            if (sccIndex != next.sccIndex) {
                return false;
            }
        }
        return true;
    }
}

public class Asn4B {
    private static void defineOrder(Tile tile, boolean visited[], Stack<Tile> stack) {
        visited[tile.index] = true;
        for (Tile next : tile.outEdges) {
            if (!visited[next.index]) {
                defineOrder(next, visited, stack);
            }
        }
        stack.push(tile);
    }

    private static void groupSCCs(Tile tile, boolean visited[], int sccIndex) {
        visited[tile.index] = true;
        tile.sccIndex = sccIndex;
        for (Tile next : tile.inEdges) {
            if (!visited[next.index]) {
                groupSCCs(next, visited, sccIndex);
            }
        }
    }

    public static void main(String _args[]) throws Exception {
        final BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        final PrintWriter printWriter = new PrintWriter(System.out);

        // Find the number of SCCs without an in-edge from outside the SCC.
        final int numTestCases = Integer.parseInt(bufferedReader.readLine().split(" ")[0]);
        for (int i = 0; i < numTestCases; ++i) {
            String[] firstLine = bufferedReader.readLine().split(" ");
            final int numTiles = Integer.parseInt(firstLine[0]);
            final int numLines = Integer.parseInt(firstLine[1]);

            Tile tiles[] = new Tile[numTiles];
            for (int j = 0; j < tiles.length; ++j) {
                tiles[j] = new Tile(j);
            }

            for (int j = 0; j < numLines; ++j) {
                String[] line = bufferedReader.readLine().split(" ");
                int a = Integer.parseInt(line[0]) - 1;
                int b = Integer.parseInt(line[1]) - 1;

                tiles[a].outEdges.add(tiles[b]);
                tiles[b].inEdges.add(tiles[a]);
            }

            // Kosaraju's Algorithm
            // Define the traversal order of the tiles.
            boolean visited[] = new boolean[numTiles];
            Stack<Tile> stack = new Stack<>();
            for (int j = 0; j < tiles.length; ++j) {
                Tile tile = tiles[j];
                if (!visited[tile.index]) {
                    defineOrder(tile, visited, stack);
                }
            }

            // Group the SCCs together.
            int numSCCs = 0;
            visited = new boolean[numTiles];
            while (!stack.isEmpty()) {
                Tile tile = stack.pop();
                if (!visited[tile.index]) {
                    groupSCCs(tile, visited, numSCCs++);
                }
            }

            // Count how many SCCs have no in edges from other SCCs.
            boolean isolatedSCCs[] = new boolean[numSCCs];
            Arrays.fill(isolatedSCCs, true);
            int numIsolatedSCCs = numSCCs;
            for (int j = 0; j < tiles.length; ++j) {
                Tile tile = tiles[j];
                if (isolatedSCCs[tile.sccIndex] && !tile.isIsolated()) {
                    isolatedSCCs[tile.sccIndex] = false;
                    --numIsolatedSCCs;
                }
            }

            // Print result.
            printWriter.println(numIsolatedSCCs);
        }

        bufferedReader.close();
        printWriter.close();
    }
}