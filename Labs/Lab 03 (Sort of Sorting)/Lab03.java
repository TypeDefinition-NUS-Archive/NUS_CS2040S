// Lim Ngian Xin Terry
// A0218430N

import java.util.*;

public class Lab03 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int numInputs = 0;

        do {
            // Get number of names.
            numInputs = scanner.nextInt();

            if (scanner.hasNextLine()) {
                scanner.nextLine();
            } else {
                break;
            }
    
            // Store names into an ArrayList.
            ArrayList<String> nameList = new ArrayList<String>();
            for (int i = 0; i < numInputs; ++i) {
                nameList.add(scanner.nextLine());
            }
            
            // Sort the names.
            Collections.sort(nameList, (a, b) -> {
                String lcA = a.toLowerCase();
                String lcB = b.toLowerCase();
                for (int i = 0; i < 2; i++) {
                    if (lcA.charAt(i) < lcB.charAt(i)) {
                        return -1;
                    }
                    if (lcA.charAt(i) > lcB.charAt(i)) {
                        return 1;
                    }
                }
                return 0;
            });

            // Print the names.
            nameList.forEach((name) -> { System.out.println(name); });

            // Print Blank Row
            System.out.println("");
        } while (numInputs != 0);

        scanner.close();
    }
}