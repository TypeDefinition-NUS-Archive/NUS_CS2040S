// Lim Ngian Xin Terry
// A0218430N

import java.util.*;

public class Lab02 {
    public static HashMap<Character, DialInput> createDialTable() {
        HashMap<Character, DialInput> dialTable = new HashMap<Character, DialInput>();

        insertDialMapping(dialTable, '0', ' ', ' ');
        insertDialMapping(dialTable, '2', 'a', 'c');
        insertDialMapping(dialTable, '3', 'd', 'f');
        insertDialMapping(dialTable, '4', 'g', 'i');
        insertDialMapping(dialTable, '5', 'j', 'l');
        insertDialMapping(dialTable, '6', 'm', 'o');
        insertDialMapping(dialTable, '7', 'p', 's');
        insertDialMapping(dialTable, '8', 't', 'v');
        insertDialMapping(dialTable, '9', 'w', 'z');

        return dialTable;
    }

    public static void insertDialMapping(HashMap<Character, DialInput> dialTable, char number, char startChar, char endChar) {
        for (char i = startChar; i <= endChar; i++) {
            dialTable.put(i, new DialInput(number, (int)(i - startChar) + 1));
        }
    }

    public static String scanLine(HashMap<Character, DialInput> dialTable, String input) {
        String result = new String();

        DialInput prevInput = null;
        for (int i = 0; i < input.length(); i++) {
            DialInput currInput = dialTable.get(input.charAt(i));

            if (prevInput != null && prevInput.number == currInput.number) {
                result += ' ';
            }

            for (int j  = 0; j < currInput.presses; j++) {
                result += currInput.number;
            }

            prevInput = currInput;
        }

        return result;
    }

    public static void main(String[] args) {
        HashMap<Character, DialInput> dialTable = createDialTable();

        Scanner scanner = new Scanner(System.in);
        int numInputs = scanner.nextInt();
        scanner.nextLine();

        for (int i = 0; i < numInputs; ++i) {
            String line = scanner.nextLine();
            System.out.println("Case #" + (i+1) + ": " + scanLine(dialTable, line));
        }
        scanner.close();
    }
}