// Lim Ngian Xin Terry
// A0218430N

#include <iostream>
#include <cstring>
#include <vector>

using namespace std;

enum square_type
{
    square_type_water = 0,
    square_type_land,
    square_type_clouds,
};

struct grid_node
{
    const int row, col;
    const square_type type;
    bool visited;

    grid_node()
        : row(0), col(0), type(square_type_water), visited(false)
    {
    }

    grid_node(int _row, int _col, square_type _type)
        : row(_row), col(_col), type(_type), visited(false)
    {
    }
};

void link_nodes(int _r, int _c, grid_node *_matrix, grid_node *_start_node)
{
    vector<grid_node *> queue;
    queue.push_back(_start_node);
    _start_node->visited = true;

    for (int i = 0; i < queue.size(); ++i)
    {
        int row = queue[i]->row;
        int col = queue[i]->col;

        // Add any neighbour nodes that are land or clouds into the queue. We assume clouds are land.
        left:
        {
            int neighbour_row = row;
            int neighbour_col = col - 1;
            if (neighbour_row < 0 || neighbour_col < 0 || neighbour_row >= _r || neighbour_col >= _c)
            {
                goto right;
            }

            grid_node *node = &_matrix[neighbour_row * _c + neighbour_col];
            if (node->type == square_type_water || node->visited)
            {
                goto right;
            }

            queue.push_back(node);
            node->visited = true;
        }

        right:
        {
            int neighbour_row = row;
            int neighbour_col = col + 1;
            if (neighbour_row < 0 || neighbour_col < 0 || neighbour_row >= _r || neighbour_col >= _c)
            {
                goto up;
            }

            grid_node *node = &_matrix[neighbour_row * _c + neighbour_col];
            if (node->type == square_type_water || node->visited)
            {
                goto up;
            }

            queue.push_back(node);
            node->visited = true;
        }

        up:
        {
            int neighbour_row = row - 1;
            int neighbour_col = col;
            if (neighbour_row < 0 || neighbour_col < 0 || neighbour_row >= _r || neighbour_col >= _c)
            {
                goto down;
            }

            grid_node *node = &_matrix[neighbour_row * _c + neighbour_col];
            if (node->type == square_type_water || node->visited)
            {
                goto down;
            }

            queue.push_back(node);
            node->visited = true;
        }

        down:
        {
            int neighbour_row = row + 1;
            int neighbour_col = col;
            if (neighbour_row < 0 || neighbour_col < 0 || neighbour_row >= _r || neighbour_col >= _c)
            {
                continue;
            }

            grid_node *node = &_matrix[neighbour_row * _c + neighbour_col];
            if (node->type == square_type_water || node->visited)
            {
                continue;
            }

            queue.push_back(node);
            node->visited = true;
        }
    }
}

int main()
{
    int r, c;
    cin >> r >> c;

    // Fill up the matrix.
    grid_node *matrix = new grid_node[r * c];
    char *line_buffer = new char[c];
    for (int row = 0; row < r; ++row)
    {
        cin >> line_buffer;
        for (int col = 0; col < c; ++col)
        {
            switch (line_buffer[col])
            {
            case 'W':
                new (&matrix[row * c + col]) grid_node(row, col, square_type_water);
                break;
            case 'L':
                new (&matrix[row * c + col]) grid_node(row, col, square_type_land);
                break;
            case 'C':
                new (&matrix[row * c + col]) grid_node(row, col, square_type_clouds);
                break;
            default:
                break;
            }
        }
    }

    // Find any unvisited node that is land.
    int num_islands = 0;
    for (int row = 0; row < r; ++row)
    {
        for (int col = 0; col < c; ++col)
        {
            grid_node *curr_node = &matrix[row * c + col];
            if (curr_node->visited || curr_node->type != square_type_land)
            {
                continue;
            }
            ++num_islands;
            // Set all the nodes that are land or clouds and linked to this node as visited.
            link_nodes(r, c, matrix, curr_node);
        }
    }

    // Cleanup memory.
    delete[] line_buffer;
    delete[] matrix;

    // Print answer.
    cout << num_islands << endl;

    return 0;
}