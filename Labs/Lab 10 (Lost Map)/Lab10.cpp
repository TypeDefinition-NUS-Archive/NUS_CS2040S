// Lim Ngian Xin Terry
// A0218430N

#include <vector>
#include <string>
#include <cstring>
#include <iostream>
#include <algorithm>

class ufds
{
private:
    int *rank, *parents;

public:
    const int num_elements;

    ufds(int _num_elements)
        : num_elements(_num_elements)
    {
        rank = new int[_num_elements];
        parents = new int[_num_elements];
        std::memset(rank, 0, _num_elements * sizeof(int));
        for (int i = 0; i < _num_elements; ++i)
        {
            parents[i] = i;
        }
    }

    ~ufds()
    {
        delete[] rank;
        delete[] parents;
    }

    int find_set(int _index)
    {
        return (parents[_index] == _index) ? _index : (parents[_index] = find_set(parents[_index]));
    }

    bool is_same_set(int _a, int _b)
    {
        return find_set(_a) == find_set(_b);
    }

    // Union set A and set B and return the root.
    int union_set(int _a, int _b)
    {
        // Find the set that A and B belongs to.
        int a_root = find_set(_a);
        int b_root = find_set(_b);

        // Do nothing if both elements are already in the same set.
        if (a_root == b_root)
        {
            return a_root;
        }

        // If rank[A] < rank[B], place A under B.
        if (rank[a_root] < rank[b_root])
        {
            parents[a_root] = b_root;
            return b_root;
        }

        // Else, place B under A.
        parents[b_root] = a_root;

        // If rank[A] == rank[B], the rank of A increases by one.
        if (rank[a_root] == rank[b_root])
        {
            ++rank[a_root];
        }

        return a_root;
    }
};

struct edge
{
    int src, dest, dist;

    edge(int _src, int _dest, int _dist)
        : src(_src), dest(_dest), dist(_dist)
    {
    }

    std::string to_string()
    {
        return std::to_string(src) + " " + std::to_string(dest);
    }
};

int main()
{
    int n;
    std::cin >> n;

    // Get edges.
    std::vector<edge> edges;
    edges.reserve((n*n-n)/2);
    for (int src = 0; src < n; ++src)
    {
        for (int dest = 0; dest < n; ++dest)
        {
            int dist;
            std::cin >> dist;
            if (dest <= src)
            {
                continue;
            }

            edges.push_back({1 + src, 1 + dest, dist});
        }
    }

    // Sort edges.
    std::sort(edges.begin(), edges.end(), [](const edge &_x, const edge &_y) -> bool
              { return _x.dist < _y.dist; });

    // Filter valid edges.
    ufds set{1 + n}; // Use 1 + n because our vertices index starts from 1.
    int valid_counter = 0;
    for (auto &&e : edges)
    {
        if (set.is_same_set(e.src, e.dest))
        {
            continue;
        }

        set.union_set(e.src, e.dest);
        std::cout << e.to_string() << std::endl; // Print results.

        if (++valid_counter == n - 1) // A tree with n vertices should have exactly n - 1 edges.
        {
            break;
        }
    }

    return 0;
}